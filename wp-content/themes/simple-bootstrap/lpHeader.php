<!doctype html>
<?php
$phoneNumber = '0723940811';
?>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel='shortcut icon' type='image/x-icon' href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico"/>

    <?php include_once 'headTags.php' ;?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header class="landing-page-main-header">
    <div class="flex-box">
        <div class="logo-wrap">
            <a class="logo" title="<?php bloginfo('description'); ?>"
               href="<?php echo esc_url(home_url('/lp')); ?>">
                <img width="80"
                     src="<?php echo get_bloginfo('template_directory'); ?>/img/logo_new.png"/>
                <span><?php _e("Dr. Lari Sapoznikov", "simple-bootstrap"); ?></span>
            </a>
        </div>
<!--        <div class="socials">-->
<!--            --><?php //include_once 'socials.php' ?>
<!--        </div>-->
        <div class="phone-wrap">
                <span class="phone-desktop">
                    <a href="tel:<?php _e($phoneNumber, "simple-bootstrap"); ?>">
                    <?php _e("Phone:", "simple-bootstrap"); ?><?php _e($phoneNumber, "simple-bootstrap"); ?>
                        </a>
                </span>
            <span class="phone-mobile">
                 <a href="tel:<?php _e($phoneNumber, "simple-bootstrap"); ?>">
                     <i class="fa fa-phone"></i>
                        </a>
            </span>
        </div>
    </div>
</header>
<div id="content-wrapper">
    <div id="page-content">
        <div class="container-fluid">
