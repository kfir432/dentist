var Main = {
    selectedMenuItem: null,
    init: function () {
        // Main.homeMenuSettings();
        Main.attachEvents();
        Main.scrollToTop();
        Main.scrollToTopInit();
    },
    attachEvents: function () {
       // jQuery('.header-search').click(Main.toggleSearch);
    },
    toggleSearch: function () {
        jQuery('#main-search input').toggleClass('open');
    },
    scrollToTopInit: function () {
        jQuery(".scroll-top,.scroll-top-mobile").click(function () {
            jQuery("html, body").animate({
                scrollTop: 0
            }, "slow");
            return false;
        });

    },
    scrollToTop: function () {
        jQuery(window).scroll(function () {
            jQuery('.scroll-top').hide();
            if (jQuery(this).scrollTop() > 150) {
                jQuery('.scroll-top').fadeIn();
                jQuery("nav.navbar").addClass("nav-drag");
                jQuery(".logo-search-wrapper").removeClass("fadeInDown");
                jQuery(".logo-search-wrapper").addClass("fadeOutUp");
            } else {
                jQuery('.scroll-top').fadeOut();
                jQuery("nav.navbar").removeClass("nav-drag");
                jQuery(".logo-search-wrapper").removeClass("fadeOutUp");
                jQuery(".logo-search-wrapper").addClass("fadeInDown");

            }
        });
    }

};

jQuery(document).ready(function () {
    Main.init();
});