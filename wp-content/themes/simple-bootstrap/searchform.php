<form action="<?php echo home_url( '/' ); ?>" method="get" id="main-search">
    <fieldset>
		<div class="input-group">
			<input type="text" name="s" id="search" placeholder="<?php _e("Search", "simple-bootstrap"); ?>" value="<?php the_search_query(); ?>" class="form-control open" />
		</div>
    </fieldset>
</form>