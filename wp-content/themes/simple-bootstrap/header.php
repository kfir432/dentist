<!doctype html>
<?php
$phoneNumber = '0723940811';
$email = 'doctorlaris@gmail.com';
?>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel='shortcut icon' type='image/x-icon' href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico"/>
    <link href="https://fonts.googleapis.com/css?family=Heebo" rel="stylesheet">
    <?php include_once 'headTags.php'; ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="content-wrapper" class="main-site">

    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="top-stripe">
                <div class="container">
                    <div class="contact-info-mobile">
                        <a href="tel:<?= $phoneNumber; ?>">
                            <?php _e("Phone:", "simple-bootstrap"); ?><?= $phoneNumber; ?>
                        </a>
                    </div>
                    <div class="contact-info">
                        <a href="#">
                            <i class="fa fa-map-marker"></i>
                            <?= _e('address', 'simple-bootstrap'); ?>
                        </a>
                        <a href="mailto:<?= $email ?>">
                            <i class="fa fa-envelope"></i>
                            <?= $email; ?>
                        </a>
                    </div>
                    <div class="socials">
                        <a href="https://www.facebook.com/%D7%94%D7%A9%D7%AA%D7%9C%D7%AA-%D7%A9%D7%99%D7%A0%D7%99%D7%99%D7%9D-%D7%91%D7%99%D7%95%D7%9D-%D7%90%D7%97%D7%93-%D7%93%D7%A8-%D7%9C%D7%A8%D7%99-Dr-Lari-1736013619846197/"><i
                                    class="fa fa-facebook"></i></a>
                        <a href="https://www.youtube.com/channel/UC4eHhjIhNyR4_qODvSFbwzQ"><i class="fa fa-youtube"></i></a>
                    </div>

                </div>
            </div>
            <div class="logo-search-wrapper animated fadeInDown">
                <div class="container">
                    <div class="flex-box">
                        <div class="header-call-now">
                            <span class="header-search">
                                <img width="35px"
                                     src="<?php echo get_bloginfo('template_directory'); ?>/img/search.svg"/>
                            </span>
                            <?php include_once 'searchform.php'; ?>
                            <a href="tel:<?= $phoneNumber; ?>">
                                <?php _e("Phone:", "simple-bootstrap"); ?><?= $phoneNumber; ?>
                            </a>
                        </div>
                        <a class="logo navbar-brand" title="<?php bloginfo('description'); ?>"
                           href="<?php echo esc_url(home_url('/')); ?>">
                            <span><?php _e("Dr. Lari Sapoznikov", "simple-bootstrap"); ?></span>
                            <img width="70"
                                 src="<?php echo get_bloginfo('template_directory'); ?>/img/logo-main.svg"/>
                        </a>
                    </div>


                </div>
            </div>
            <div class="header-menu-wrapper">
                <div class="container">
                    <?php if (has_nav_menu("main_nav")): ?>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-responsive-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    <?php endif ?>
                    <?php if (has_nav_menu("main_nav")): ?>
                        <div id="navbar-responsive-collapse" class="collapse navbar-collapse">
                            <?php
                            simple_bootstrap_display_main_menu();
                            ?>

                        </div>
                    <?php endif ?>
                </div>
            </div>


        </nav>
    </header>

    <?php if (has_header_image()): ?>
        <div class="header-image-container">
            <div class="header-image" style="background-image: url(<?php echo get_header_image(); ?>)">
                <div class="container">
                    <?php if (display_header_text()): ?>
                        <div class="site-title"
                             style="color: #<?php header_textcolor(); ?>;"><?php bloginfo('name') ?></div>
                        <div class="site-description"
                             style="color: #<?php header_textcolor(); ?>;"><?php bloginfo('description') ?></div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    <?php endif ?>

    <div id="page-content">
        <div class="container-fluid">
