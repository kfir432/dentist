<?php get_header(); ?>

    <div id="content" class="row">

        <div id="main" class="search-results container" role="main">

            <div class="search-header">
                <h1><?php echo _e("Search for:", "simple-bootstrap"); ?><?php echo esc_attr(get_search_query()); ?></h1>
            </div>
            <div class="col-xs-8">
                <?php if (have_posts()) : ?>

                    <?php while (have_posts()) : the_post(); ?>
                        <div class="search-row row">
                            <?php simple_boostrap_display_post(true); ?>
                        </div>

                    <?php endwhile; ?>


                <?php else : ?>

                    <!-- this area shows up if there are no results -->

                    <article id="post-not-found" class="search-row">
                        <p><?php _e("No items found.", "simple-bootstrap"); ?></p>
                    </article>

                <?php endif; ?>
            </div>


        </div>

    </div>

<?php get_footer(); ?>