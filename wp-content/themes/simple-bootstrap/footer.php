</div>
</div>

<footer>
    <div id="inner-footer" class="vertical-nav">
        <div class="container">
            <div class="row">
                <?php dynamic_sidebar('footer1'); ?>

                <div class="col-xs-12 text-center copyright">
                    <p><?php _e("copyrights", "simple-bootstrap"); ?>&nbsp;<?php echo date("Y"); ?>
                        <br/>
                        <a target="_blank"
                           href="http://www.we-do.media/"><?php _e("wedomedia", "simple-bootstrap"); ?></a>
                    </p>

                </div>
            </div>
        </div>
    </div>
</footer>

</div>
<div id="tooper">
    <div class="grid col-300 scroll-top"><a href="#scroll-top"
                                            title="<?php esc_attr_e('scroll to top', 'simple-bootstrap'); ?>"><i
                    class="fa fa-chevron-up"></i></a></div>
</div>
<?php wp_footer(); // js scripts are inserted using this function ?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.1&appId=143315755762761&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</body>

</html>
