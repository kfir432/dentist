<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lp_drLarry_db');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8}F.cu^XTiK.L::#=Tc5MOWp8Znj4W<WO:{sIalb5Q_hXt ?/Y%L([$](Hjw~hXH');
define('SECURE_AUTH_KEY',  'kxOnn(/m?M6+u.##[QW>@Wu3shVxb4A?GJoyrPH&3{2ehp?+}Il:VP@GVZ][.2#Z');
define('LOGGED_IN_KEY',    '>|BQ]M+<2Qy|XHo%p&wvp#If<JIODcAI=Xf(*%H`;Y}Dc+c.I[o@y@{Fb,2zpx,X');
define('NONCE_KEY',        '?4wi=ZS{^R_kIg,IZbqNP-+&}+y%w+&XibB=Qdzvha$;I(cyPI@2O-nW)(3W2!10');
define('AUTH_SALT',        ';&b=vV?@.zWeMmCn!krB_x4!hVnJ6fzt_%#+YvbV;38-HHIh^R&[R_=x- T*AuZd');
define('SECURE_AUTH_SALT', 's4;-^/19yu|1M+-!cXsnQcq/J/h[7v0L5^@btzqxswpO P#+;~m_Dp-JV&*fQTM!');
define('LOGGED_IN_SALT',   '2CMEH9n6ZBw#oMN6RvSIG$=(8@sU]^=`H8at{2{.:faC2)jyt3F`.d-LK>I@Z:g|');
define('NONCE_SALT',       'p8X7xPF*cYTIm>OFc6tjVE8y}i;b@U[l|{EFTAWdeLI=+0-51KvsM!QU]{.|J+a)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
