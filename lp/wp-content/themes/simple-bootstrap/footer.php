		
    	   </div>
        </div>
            
        <footer>
            <div id="inner-footer" class="vertical-nav">
                <div class="container">
                    <div class="row">
                        <?php dynamic_sidebar('footer1'); ?>

                        <div class="col-xs-12 text-center copyright">
                            <p><?php _e("copyrights", "simple-bootstrap"); ?>&nbsp;<?php echo date("Y"); ?>
                                <br/>
                                <a target="_blank" href="http://www.we-do.media/"><?php _e("wedomedia", "simple-bootstrap"); ?></a>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>

	<?php wp_footer(); // js scripts are inserted using this function ?>

</body>

</html>
