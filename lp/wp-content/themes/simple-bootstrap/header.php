<!doctype html>
<?php
$phoneNumber = '0723940811';
$email = 'mail@mail.com';
?>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel='shortcut icon' type='image/x-icon' href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico"/>
    <?php include_once 'headTags.php'; ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="content-wrapper" class="main-site">

    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="top-stripe">
                <div class="container">
                    <div class="contact-info">
                        <a href="#">
                            <i class="fa fa-map-marker"></i>
                            <?= _e('address', 'simple-bootstrap'); ?>
                        </a>
                        <a href="mailto:<?= $email ?>">
                            <i class="fa fa-envelope"></i>
                            <?= $email; ?>
                        </a>


                    </div>
                    <div class="socials">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                    </div>

                </div>
            </div>
            <div class="logo-search-wrapper">
                <div class="container">
                    <div class="flex-box">
                        <div class="header-call-now">
                            <?php include_once 'searchform.php'; ?>
                            <a href="tel:<?= $phoneNumber; ?>">
                                <?php _e("Phone:", "simple-bootstrap"); ?><?= $phoneNumber; ?>
                            </a>
                        </div>
                        <a class="logo navbar-brand" title="<?php bloginfo('description'); ?>"
                           href="<?php echo esc_url(home_url('/lp')); ?>">
                            <span><?php _e("Dr. Lari Sapoznikov", "simple-bootstrap"); ?></span>
                            <img width="70"
                                 src="<?php echo get_bloginfo('template_directory'); ?>/img/logo-main.svg"/>
                        </a>
                    </div>


                </div>
            </div>
            <div class="header-menu-wrapper">
                <div class="container">
                    <?php if (has_nav_menu("main_nav")): ?>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-responsive-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    <?php endif ?>
                    <?php if (has_nav_menu("main_nav")): ?>
                        <div id="navbar-responsive-collapse" class="collapse navbar-collapse">
                            <?php
                            simple_bootstrap_display_main_menu();
                            ?>

                        </div>
                    <?php endif ?>
                </div>
            </div>


        </nav>
    </header>

    <?php if (has_header_image()): ?>
        <div class="header-image-container">
            <div class="header-image" style="background-image: url(<?php echo get_header_image(); ?>)">
                <div class="container">
                    <?php if (display_header_text()): ?>
                        <div class="site-title"
                             style="color: #<?php header_textcolor(); ?>;"><?php bloginfo('name') ?></div>
                        <div class="site-description"
                             style="color: #<?php header_textcolor(); ?>;"><?php bloginfo('description') ?></div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    <?php endif ?>

    <div id="page-content">
        <div class="container-fluid">
