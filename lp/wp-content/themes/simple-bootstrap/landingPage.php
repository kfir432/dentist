<?php
/*
Template Name: Landing Page
*/
?>
<?php include_once 'lpHeader.php'?>

<div id="content" class="row">

	<div id="landing-page" role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<?php simple_boostrap_display_post(false); ?>
		
		<?php comments_template('',true); ?>
		
		<?php endwhile; ?>		
		
		<?php else : ?>
		
		<article id="post-not-found" class="block">
		    <p><?php _e("No pages found.", "simple-bootstrap"); ?></p>
		</article>
		
		<?php endif; ?>

	</div>

</div>

<?php get_footer(); ?>